using System;
using System.Collections.Generic;
using Autofac;
using Microsoft.Practices.ServiceLocation;

namespace Prism.AutofacExtension
{
  /// <summary>
  /// A autofac service locator adapter implementation
  /// </summary>
  public class AutofacServiceLocatorAdapter : ServiceLocatorImplBase
  {
    private readonly IComponentContext _container;

    /// <summary>
    /// Initializes a new instance of the <see cref="AutofacServiceLocatorAdapter"/> class.
    /// </summary>
    /// <param name="container">The container.</param>
    public AutofacServiceLocatorAdapter(IComponentContext container)
    {
      _container = container;
    }

    /// <summary>
    /// When implemented by inheriting classes, this method will do the actual work of resolving
    /// the requested service instance.
    /// </summary>
    /// <param name="serviceType">Type of instance requested.</param>
    /// <param name="key">Name of registered service you want. May be null.</param>
    /// <returns>
    /// The requested service instance.
    /// </returns>
    protected override object DoGetInstance(Type serviceType, string key)
    {
      object resolvedObject;
      if (string.IsNullOrEmpty(key))
      {
        if (_container.TryResolve(serviceType, out resolvedObject))
        {
          return resolvedObject;
        }
      }
      else
      {
        if (_container.TryResolveNamed(key, serviceType, out resolvedObject))
        {
          return resolvedObject;
        }  
      }
      
      throw new Exception(string.Format("cannot resolve type {0}", serviceType));
    }

    /// <summary>
    /// When implemented by inheriting classes, this method will do the actual work of
    /// resolving all the requested service instances.
    /// </summary>
    /// <param name="serviceType">Type of service requested.</param>
    /// <returns>
    /// Sequence of service instance objects.
    /// </returns>
    protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
    {
      return new object[] { this._container.Resolve(serviceType) };
    }
  }
}